@extends('Nightout.layouts.master_layout')
@section('content')
<link rel="stylesheet" type="text/css" href="/Assets/bootstrap-datepicker-master/dist/css/bootstrap-datepicker.min.css">
<section class="content" ng-controller="VisitorsController">
    <div class="container-fluid">
        <div class="block-header">
        </div>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header form-inline ">
                     <h2> Import Student Details</h2> 
                                              
        <form name=" MaatwebsiteDemo" style="margin-top: 15px;padding: 10px;" action="{{ URL::to('/office/importExcel') }}" class="from_company" method="post" enctype="multipart/form-data">
        {{csrf_field()}}
            <input type="file"  name="import_file" style="padding-bottom: 5px;"/>

            <button name="import"  class="btn btn-primary" style="        margin-top: -48px;
    margin-left: 200px;">Submit</button>
           
 <a href="/sample.xlsx" class="btn btn-info" style="float:right;
    margin-top: -25px;">Sample Excel File</a>
        </form>

          @if(Session::has('login-Success'))
                        <p  style="    color: green;">{{ Session::get('login-Success') }}</p>
                        @endif
                          @if(Session::has('login-Error'))

                        <p  style="    color: red;">{{ Session::get('login-Error') }}</p>
                        @endif
        </div>


        
              <div class="table-responsive body">
         <table class="table table-bordered table-striped table-hover ">
                            <thead>
                                <tr align="center">
                                    <th width="5%">Sl No</th>
                                             
                                             <th width="5%">Roll_No</th>
                                    <th width="5%">Name_Of_The Student</th>
                                            <th>Sex</th>
                                            
                                            <th>Email_Id</th>
                                            <th>Mobile_No</th>
                                            <th>Terms Credit</th>
                                            <th>CFR</th>
                                            <th>PGP</th>
                                            <th>Placed</th>
                                            <!-- 
                                            <th>Request_Date &Time </th>
                                            <th>Placed & Unplaced</th> -->
                                            
                                </tr>
                            </thead>
      
                            <tbody align="center">
                                <tr ng-repeat="visitor in visitors" id="<% visitor._id %>">
                                    <th scope="row"><%(visitorsPerPage * (currentPage-1)) + $index+1  %></th>
                                   
                                    <td><%visitor.rollNo %></td>
                                    <td><%visitor.name %></td>
                                                                     
                                     <td><%visitor.gender %></td>
                                       <td><%visitor.email %></td>
                                      <td><%visitor.mobileNumber %></td>
                                     
                                            <td><%visitor.termCredits %></td>
                                       <td><%visitor.carryForward %></td>

                                       
                                        <td><%visitor.pgp %></td>
                                        <td ng-if='visitor.placed=="1"'>Yes</td>
                                        <td ng-if='visitor.placed=="0"'>No</td>
                                     
                                      <!--    <td><%visitor.roomNumber %></td> -->



                                      
                                <tr ng-hide="visitors.length">
                                    <td colspan="8" align="center"><b>No visitors to display</b></td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="row">
                          <div class="col-md-6">
                            <div >Showing <%visitorsFrom %> to <%visitorsTo%> of <% totalVisitors %> entries</div>
                          </div>
                          <div class="col-md-6 text-right">
                            <ul uib-pagination total-items="totalVisitors" ng-model="currentPage" class="pagination-sm" boundary-links="true" rotate="false" max-size="maxSize" items-per-page="visitorsPerPage"></ul>
                          </div>
                        </div>
                    </div>
                
                        
            </div>
        </div>
    </div>
      </div>

</section>
<script src="{{asset('Office/excel_controls.js')}}"></script>

<script src="{{asset('Assets/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js')}}"></script>
<script type="text/javascript">
    $('#datepicker').datepicker({
        format: 'dd-mm-yyyy'
    });
</script>
@endsection