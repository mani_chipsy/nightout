<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <link rel="stylesheet" type="text/css" href="Backend/webadmin/css/custom.css">
    </head>
    <body>
        <div class="wrapper pass">
            <header>
               <table align="center">
                    <tr>
                        <td class="brand-left"><img border="0" src="Backend/webadmin/images/tapmi-logo.png"/></td>
                        <td><h2>T A PAI MANAGEMENT INSTITUTE - MANIPAL</h2></td>
                        <td class="brand-right"><img border="0" src="Backend/webadmin/images/tapmi-AACSB.png"  /></td>
                    </tr>
                </table>
            </header> 
            <div class="maincontent">
                <h3 style="text-align: center; text-decoration: underline;">Visitor's Pass</h3>
                <table align="center" width="100%">
                    <tr>
                        <td width="15%" >Name</td>
                        <td width="20%" colspan='2' style="border-bottom: 1px solid black">{{$visitor->name}}</td>
                        <td width="25%">Mobile Number</td>
                        <td width="40%" colspan='2'  style="border-bottom: 1px solid black">{{$visitor->mobile_num}}</td>
                    </tr>
                    <tr>
                        <td width="15%" >Address</td>
                        <td width="85%" colspan='5'  style="border-bottom: 1px solid black">{{$visitor->address}}</td>
                    </tr>
                    <tr>
                        <td width="15%" >Requested by</td>
                        <td width="85%" colspan='5'  style="border-bottom: 1px solid black">
                            @if ($visitor->requester)
                                {{$visitor->requester->username}}
                            @else Security @endif
                        </td>
                    </tr>
                    <tr>
                        <td width="15%" colspan='2' >Whom to meet in TAPMI</td>
                        <td width="85%" colspan='4'  style="border-bottom: 1px solid black">{{$visitor->whom_to_meet}}</td>
                    </tr>
                    <tr>
                        <td width="25%" >Purpose of visit</td>
                        <td width="75%" colspan='5'  style="border-bottom: 1px solid black">{{$visitor->purpose}}</td>
                    </tr>
                    <tr>
                        <td width="20%" >Date of visit</td>
                        <td width="80%"  colspan='5' style="border-bottom: 1px solid black">{{$visitor->date_of_visit}}</td>
                        {{--<td width="5%" align="right">Time in</td>
                        <td width="15%" style="left:-10px;border-bottom: 1px solid black">&nbsp;</td>
                        <td width="5%"  align="right">Time out</td>
                        <td width="30%" style="left:-10px;border-bottom: 1px solid black">&nbsp;</td>--}}
                    </tr>
                </table>
                {{--<table align="center" style="margin-top:60px" width="100%">
                    <tr>
                        <td style="border-bottom: 1px solid black">&nbsp;</td>
                        <td width="10%">&nbsp;</td>
                        <td style="border-bottom: 1px solid black">&nbsp;</td>
                        <td width="10%">&nbsp;</td>
                        <td style="border-bottom: 1px solid black">&nbsp;</td>
                    </tr>
                    <tr>
                        <td width="20%">Visitor's Signature</td>
                        <td width="10%">&nbsp;</td>
                        <td width="20%">Security Supervisor</td>
                        <td width="10%">&nbsp;</td>
                        <td width="40%">Signature of the person met in TAPMI</td>
                    </tr>
                </table>--}}
                {{--<div class="row-padding" style="margin-top:60px" >
                    <div class="row">
                        <div class="col-md-6 row-padding">Photo 1.</div>
                        <div class="col-md-6 row-padding">Photo 2.</div>
                    </div>
                    <div class="row" >
                        <div class="col-md-6">
                            @if ($visitor->visitor_img)
                                    <img class="img-responsive" style="width:250px;height:500px;border:1px solid #021a40" src="{{$visitor->visitor_img}}"  />
                                @else
                                    <img class="img-responsive" style="width:250px;height:500px;border:1px solid #021a40" src="Backend/webadmin/images/vistor-photo.png"  />
                                @endif
                        </div>
                        <div class="col-md-6">
                            <img class="img-responsive" style="height:250px;border:1px solid #021a40" src="Backend/webadmin/images/vistor-photo.png"  />
                        </div>
                    </div>
                </div>--}}
                <table class="avatar" cellspacing="10px" align="center" style="margin-top:60px" width="100%" >
                    <tr>
                        <td colspan="3" style="text-align:center">Photo 1.</td>
                        <td colspan="3" style="text-align:center">Photo 2.</td>
                    </tr>
                    <tr >
                        <td colspan="3" class="brand-right"  style="width: 
                        250px; height: 400px">
                            @if ($visitor->visitor_img)
                                <img style=" height:400px;border:1px solid #021a40" src="{{$visitor->visitor_img}}"  />
                            @else
                                <img style=" height:400px;border:1px solid #021a40" src="Backend/webadmin/images/vistor-photo.png"  />
                            @endif
                        </td>
                        <td colspan="3" class="brand-right"  style="width: 250px; height: 400px;">
                            @if ($visitor->visitor_id_img)
                                <img style=" height:400px;border:1px solid #021a40" src="{{$visitor->visitor_id_img}}"  />
                            @else
                                <img style=" height:400px;border:1px solid #021a40" src="Backend/webadmin/images/vistor-photo.png"  />
                            @endif
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </body>
</html>