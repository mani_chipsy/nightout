<!DOCTYPE html>
<html ng-app="nightout">
    @include('Nightout.header.header')
    <body class="theme-red">
        <style type="text/css">
            .content table {
            font-size: 12px !important; 
            }
        </style>
        <!-- Page Loader -->
        <div class="page-loader-wrapper">
            <div class="loader">
                <div class="preloader">
                    <div class="spinner-layer pl-red">
                        <div class="circle-clipper left">
                            <div class="circle"></div>
                        </div>
                        <div class="circle-clipper right">
                            <div class="circle"></div>
                        </div>
                    </div>
                </div>
                <p>Please wait...</p>
            </div>
        </div>
        <!-- #END# Page Loader -->
        <!-- Overlay For Sidebars -->
        <div class="overlay"></div>
        <!-- #END# Overlay For Sidebars -->
        <!-- Top Bar -->
        <nav class="navbar">
            <div class="container chng-width-navbar">
                <div class="navbar-header">
                    <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                    <!-- <a class="navbar-brand" href="/vms/visitors">
                        <img src="/Backend/webadmin/images/tapmi-logo.png" alt=""> 
                        Tapmi VMS
                    </a> -->
                     <?php $i = Request::segment(1) ?>
                                               
                          @if ($i=="superadmin")
                           <a class="navbar-brand" href="/superadmin/dashbord">
                        <img src="/Backend/webadmin/images/tapmi-brand.png" width="40px" style="    margin-left: 42px;" />  
                          <b style="text-transform: capitalize;"> Tapmi {{$i}} </b>
                    </a>
                    @elseif ($i=="nightout")
                    <a class="navbar-brand" href="/nightout/pgp-office/dashbord">
                        <img src="/Backend/webadmin/images/tapmi-brand.png" width="40px" style="    margin-left: 26px;" />  
                          <b style="text-transform: capitalize;"> Tapmi {{$i}} </b>
                    </a>
                     @elseif ($i=="office")
                    <a class="navbar-brand" href="/office/excel">
                        <img src="/Backend/webadmin/images/tapmi-brand.png" width="40px" style="    margin-left: 26px;" />  
                          <b style="text-transform: capitalize;"> Tapmi {{$i}} </b>
                    </a>
                    @else
                     <a class="navbar-brand" href="/warden/dashbord">
                        <img src="/Backend/webadmin/images/tapmi-brand.png" width="40px" style="    margin-left: 26px;" />  
                          <b style="text-transform: capitalize;"> Tapmi {{$i}} </b>
                    </a>

                    @endif
                </div>                
                <div class="collapse navbar-collapse" id="navbar-collapse">
                    <ul class="nav navbar-nav">
                     @if ($i=="nightout")
                       <li>
                                <a href="/nightout/pgp-office/dashbord">
                                  Dashbord
                                </a>
                            </li>
                            
                          <li>
                            <a href="/nightout/pgp-office/request">
                                  Home
                                </a>
                            </li>

                                @elseif ($i=="superadmin")
                                 <li>
                                <a href="/superadmin/dashbord">
                                  Dashbord
                                </a>
                            </li>
                                 <li>
                                <a href="/superadmin/create">
                                  Wardens
                                </a>
                            </li>
                            
                               <li>
                                <a href="/superadmin/display">
                                 Students
                                </a>
                            </li>
                              <li>
                                <a href="/superadmin/nightout">
                                 Nightout
                                </a>
                            </li>

                             @elseif ($i=="office")
                             
                            <li>
                                <a href="/office/excel">
                                    Home
                                </a>
                            </li>
                            @else
                               <li>
                                <a href="/warden/dashbord">
                                    Dashbord
                                </a>
                            </li>
                             <li>
                                <a href="/warden/nightout">
                                  Wardens
                                </a>
                            </li>
                           
                            @endif

                     
                    </ul>
                    <ul class="nav navbar-nav navbar-right" id="nav-collapse4">
                        {{--    <li>
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                                <i class="material-icons">notifications</i>
                                <span class="label-count">7</span>
                            </a>
                            <ul class="dropdown-menu">
                            </ul>
                        </li>--}}
                        <li class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> <span class="glyphicon glyphicon-user"></span> 
                        <strong>{{Auth::guard($i)->user()->username}}</strong>
                        <span class="caret"></span></a>
                          <ul class="dropdown-menu" role="menu">
                           @if ($i=="nightout")
                                <li><a href="/nightout/pgp-office/settings">Settings</a></li>
                                <li class="divider"></li>
                                <li><a href="/nightout/pgp-office/signout">Logout</a></li>
                           @elseif ($i=="superadmin")
                                <li><a href="/superadmin/settings">Settings</a></li>
                                <li class="divider"></li>
                                <li><a href="/superadmin/signout">Logout</a></li>
                             @elseif ($i=="office")
                              <li><a href="/office/settings">Settings</a></li>
                            <li class="divider"></li>
                            <li><a href="/office/signout">Logout</a></li>
                            @else
                            <li><a href="/warden/settings">Settings</a></li>
                            <li class="divider"></li>
                            <li><a href="/warden/signout">Logout</a></li>
                             @endif
                          </ul>
                        </li>
                    </ul>                    
                </div>
            </div>
        </nav>
         <script type="text/javascript"> 

         var custom_url="<?php  echo $i; ?>";
         </script>
        <div class="container chg-width">
            @yield('content')
        </div>

        {{--<footer>
          <div class="container">
            <p class="text-p text-right">&copy; {{date('Y')}} <a href="http://chipsy.in">chipsy.in Design</a>. All rights reserved.
            <!--<a href="https://www.facebook.com/Helsespesialisten-448789105236638/" target="_blank"><i class="fa fa-facebook"></i>Follow us on Facebook</a>-->
          </div>
        </footer>--}}
       
    </body>
</html>