@extends('Nightout.layouts.master_layout')
@section('content')
<link rel="stylesheet" type="text/css" href="/Assets/bootstrap-datepicker-master/dist/css/bootstrap-datepicker.min.css">
<link rel="stylesheet" type="text/css" href="{{asset('Assets/parsley/parsley.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('Assets/jquery-confirm/jquery-confirm.css')}}">
<section class="content" ng-controller="VisitorsController">
    <div class="container-fluid">
        <div class="block-header">
        </div>
        
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header form-inline">
                        <h2>
                            <span>Request Nightout (<%totalVisitors%>)</span>
                             <p class="pull-right">
                                <label for="search" class="small-label">Search: </label>
                                <select name="pgp" class="form-control show-tick">
                                    <option value="0">Requested</option>
                                    <option value="1">PGP1</option>
                                    <option value="2">PGP2</option>
                                </select>
                               
                               
                            </p>
                         <!--    <p class="pull-right">
                                <label for="search" class="small-label">Search: </label> -->
                                <!-- <select name="status" class="form-control show-tick">
                                    <option value="">Requested</option>
                                    <option value="1">Visited</option>
                                    <option value="0">Non visited</option>
                                </select> -->
                             <!--    <input type="text" class="form-control" name="search-visitor" title="Type any details" placeholder="Type any details" id="search" >
                               
                            </p> -->
                        </h2>

                    </div>
                 <div class=" body">
         <table class="table table-bordered table-striped table-hover ">
                            <thead>
                                <tr>
                                    <th width="5%">Sl No</th>
                                             <!-- <th width="5%">User_id</th> -->
                                             <th width="5%">Roll_No</th>
                                    <th width="5%">Name_Of_The Student</th>
                                            <th>Sex</th>
                                            
                                            <th>Email_Id</th>
                                            <th>Mobile_No</th>
                                            <th>Terms Credit</th>
                                            <th>CFR</th>
                                            <!-- 
                                            <th>Request_Date &Time </th>
                                            <th>Placed & Unplaced</th> -->
                                             <th>Place of Visit </th>
                                              <th>Purpose of Visit </th>
                                                <th width="15%"> Request_Date  </th>
                                                 <th width="15%"> Return_Date </th>
                                             

                                              <!--  <th>Alternate Contact Number</th> -->
                                               
                                                 <th>Hostel Number</th>
                                                  <th>Room Number</th>
                                                 <!--   <th>Declaration acceptance by the Student</th>
                                                    <th>No of Late Enteries</th> -->
                                                     <th >Status</th>
                                                   <!--  <th>More</th> -->
                                </tr>
                            </thead>
      
                            <tbody>
                                <tr ng-repeat="visitor in visitors" id="<% visitor._id %>">
                                    <th scope="row"><%(visitorsPerPage * (currentPage-1)) + $index+1  %></th>
                                   <!--  <td ><% visitor._id %></td> -->
                                    <td><%visitor.requester.rollNo %></td>
                                    <td><%visitor.requester.name %></td>
                                                                     
                                     <td><%visitor.requester.gender %></td>
                                       <td><%visitor.requester.email %></td>
                                      <td><%visitor.requester.mobileNumber %></td>
                                     
                                            <td><%visitor.requester.termCredits %></td>
                                       <td><%visitor.requester.carryForward %></td>

                                       
                                        <td><%visitor.placeOfVisit %></td>
                                     <td><%visitor.purposeVisit %></td>
                                      <td><% visitor.requested_on1 %></td>
                                     <td><% visitor.requested_on %></td>
                                        <td><%visitor.hosteNumber %></td>
                                         <td><%visitor.roomNumber %></td>



                                        <td >

                                      

                                     <div class="dropdown">
  <button class="btn btn-info dropdown-toggle" type="button" data-toggle="dropdown">Action
  <span class="caret"></span></button>
  <ul class="dropdown-menu status" id="<% visitor._id %>" >
 
   <li    id="-2"   ><a href="javascript:void(0);"  >Class</a></li>
   
 
    <li  id="1"><a href="javascript:void(0);" >No Class</a></li>
   
  
  </ul>
</div>  
                                    <!-- <select name="status" class="form-control show-tick" style="    width: fit-content;">
                                       <option value="">Requested</option>
                                    <option value="1">Class</option>
                                    <option value="0">No Class</option>
                                    </select> -->

                                      
                                      
                                       </td>

                                  
                                    <!--  <td>@{{ visitor.purposeVisit }} </td>
                                    <td>@{{ visitor.fromDate }} </td>
                                       <td>@{{ visitor.toDate }} </td>
                                     <td>@{{ visitor.hosteNumber }}</td>
                                       <td>@{{ visitor.roomNumber }} </td> --> 
                                <!--     <td ng-if="visitor.requester.username"><% visitor.requester.username %> </td>
                                    <td ng-if="!visitor.requester">
                                        Security 
                                    </td>
                                    <td><% visitor.requested_on %> </td> -->
                                    <!-- <td>
                                        <a href="javascript:void(0);" ng-click="previewCard($index)">
                                            <i class="fa fa-address-card"></i>
                                        </a>
                                    </td> -->
                                  <!--   <td>   <a class="dt-button buttons-print" tabindex="0" aria-controls="DataTables_Table_1" href="/vms/print-pass/<%visitor._id%>" target="_blank"><span>Print</span></a>  </td> -->
                                </tr>
                                <tr ng-hide="visitors.length">
                                    <td colspan="8" align="center"><b>No visitors to display</b></td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="row">
                          <div class="col-md-6">
                            <div >Showing <%visitorsFrom %> to <%visitorsTo%> of <% totalVisitors %> entries</div>
                          </div>
                          <div class="col-md-6 text-right">
                            <ul uib-pagination total-items="totalVisitors" ng-model="currentPage" class="pagination-sm" boundary-links="true" rotate="false" max-size="maxSize" items-per-page="visitorsPerPage"></ul>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade product_view" id="product_view">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <a href="#" data-dismiss="modal" class="class pull-right"><span class="glyphicon glyphicon-remove"></span></a>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<script src="{{asset('Nightout/ng-controls/nightout_controls.js')}}"></script>
<script src="{{asset('Assets/parsley/parsley.js')}}"></script>
<script src="{{asset('Assets/jquery-confirm/jquery-confirm.js')}}"></script>
<!-- <script type="text/javascript">
    $('#datepicker').datepicker({
        format: 'dd-mm-yyyy'
    });
</script> -->
@endsection