 @extends('Nightout.layouts.master_layout')
@section('content')
<link rel="stylesheet" type="text/css" href="/Assets/bootstrap-datepicker-master/dist/css/bootstrap-datepicker.min.css">
<link rel="stylesheet" type="text/css" href="{{asset('Assets/parsley/parsley.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('Assets/jquery-confirm/jquery-confirm.css')}}">
<section class="content" ng-controller="VisitorsController">
    <div class="container-fluid">
        <div class="block-header">
        </div>
        
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
                <div class="card">
                    <div class="header form-inline">
                        <h2>
                            <span>Students (<%totalVisitors%>)</span>
                            <p class="pull-right">
                                <label for="search" class="small-label">Search: </label>
                                <select name="status" class="form-control show-tick">
                                    <option value="">Requested</option>
                                    <option value="1">PGP1</option>
                                    <option value="2">PGP2</option>
                                </select>
                               
                               
                            </p>
                        </h2>

                    </div>
                 <div class=" body">
         <table class="table table-bordered table-striped table-hover ">
                            <thead>
                                <tr>
                                    <th width="5%">Sl No</th>
                                             
                                             <th width="5%">Roll_No</th>
                                    <th width="5%">Name_Of_The Student</th>
                                            <th>Sex</th>
                                            
                                           
                                            <th>Email_Id</th>
                                            <th>Mobile_No</th>
                                            <th>Terms Credit</th>
                                            <th>CFR</th>
                                            <th>PGP</th>
                                            <th>Placed</th>
                                </tr>
                            </thead>

                              <tbody align="center">
                                <tr ng-repeat="visitor in visitors" id="<% visitor._id %>">
                                    <th scope="row"><%(visitorsPerPage * (currentPage-1)) + $index+1  %></th>
                                   
                                    <td><%visitor.rollNo %></td>
                                    <td><%visitor.name %></td>
                                                                     
                                     <td><%visitor.gender %></td>
                                       <td><%visitor.email %></td>
                                      <td><%visitor.mobileNumber %></td>
                                     
                                            <td><%visitor.termCredits %></td>
                                       <td><%visitor.carryForward %></td>

                                       
                                        <td><%visitor.pgp %></td>
                                        <td ng-if='visitor.placed=="1"'>Yes</td>
                                        <td ng-if='visitor.placed=="0"'>No</td>
                                     
                                      <!--    <td><%visitor.roomNumber %></td> -->



                                      
                                <tr ng-hide="visitors.length">
                                    <td colspan="8" align="center"><b>No visitors to display</b></td>
                                </tr>
                            </tbody>
      
                          
                        </table>
                        <div class="row">
                          <div class="col-md-6">
                            <div >Showing <%visitorsFrom %> to <%visitorsTo%> of <% totalVisitors %> entries</div>
                          </div>
                          <div class="col-md-6 text-right">
                            <ul uib-pagination total-items="totalVisitors" ng-model="currentPage" class="pagination-sm" boundary-links="true" rotate="false" max-size="maxSize" items-per-page="visitorsPerPage"></ul>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade product_view" id="product_view">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <a href="#" data-dismiss="modal" class="class pull-right"><span class="glyphicon glyphicon-remove"></span></a>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<script src="{{asset('Superadmin/ng-controls/nt_controls.js')}}"></script>
<script src="{{asset('Assets/parsley/parsley.js')}}"></script>
<script src="{{asset('Assets/jquery-confirm/jquery-confirm.js')}}"></script>
<!-- <script type="text/javascript">
    $('#datepicker').datepicker({
        format: 'dd-mm-yyyy'
    });
</script> -->
@endsection