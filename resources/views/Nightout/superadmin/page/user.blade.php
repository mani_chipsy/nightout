
    @extends('Nightout.layouts.master_layout')
@section('content')
<link rel="stylesheet" type="text/css" href="{{asset('Assets/parsley/parsley.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('Assets/jquery-confirm/jquery-confirm.css')}}">
    <section class="content" ng-controller="userController">
        <div class="container-fluid">
            <div class="block-header">
                 
                
            </div>
               <!--  <h2>
                    JQUERY DATATABLES
                    <small>Taken from <a href="https://datatables.net/" target="_blank">datatables.net</a></small>
                </h2> -->
            </div>
            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12  col-xs-12">
                    <div class="card ">
                        <div class="header">
                         <!--    <h2>
                              Warden Details


                            </h2> -->
                            </div>




<div class="row clearfix" style="    margin-left: 10px;
    margin-right: 10px;">
                <div class="col-lg-5 col-md-5 col-sm-5   ">
                    <div class="card ">
                              <div class="body">
   <h2 style="margin-bottom: 30px">
                                Warden Forms<br>
                            
                            </h2>
                          <form ng-enter="profile()" name='user_profile'>
            {{csrf_field()}}
 <div class="form-group">
    <label for="email">Email address:</label>
        <input type="email"  required="required" name="email" id="email" class="form-control" placeholder="Email" data-parsley-errors-container="#email_format_error">
    
  </div>
 
                 
                         <!--    <div class="form-group">
                                <div class="form-line">
                                <label for="pwd">Password:</label>
                                    <input type="password"  id="pwd" required="required" name="username" class="form-control" placeholder="Username">
                                </div>
                              
                            </div>

                                 <div class="form-group">
                                <div class="form-line">
                                <label for="pwd">Confirm:</label>
                                    <input type="password"  id="pwd" required="required" name="username" class="form-control" placeholder="Username">
                                </div>
                              
                            </div> -->

                              
                          
                            <div class="form-group">
                            <label for="new_password">New Password</label>
                            <input type="password"  required="required" name="new_password" id="new_password" class="form-control" placeholder="New Password" data-parsley-errors-container="#new_pass_error">
    
                           
                            
                            <div id="new_pass_error"></div>
                            </div>
                       
                            <div class="form-group">
                             <label for="confirm_password">Confirm Password</label>
                            <input type="password"  value="" required="required" data-parsley-equalto="#new_password" data-parsley-equalto-message="Confirm Password should match the New Password" name="confirm_password" id="confirm_password" class="form-control" placeholder="Repeat Password" data-parsley-errors-container="#rep_pass_error">

                            <div id="rep_pass_error"></div>
                            </div>


 <div class="form-group">

                       <label class="form-label">Sex</label>
                       <div id="sex"  class="demo-radio-button">
          <input   required="required"   name="group1" type="radio" id="radio_1" value="1" />
                       <label for="radio_1">Male</label>
                       <input  required="required" name="group1" type="radio" id="radio_2" value="0"/>
                       <label for="radio_2">Female</label>
                                
                            </div>
                            </div>
 
              <div class="form-group">
                                
                                  <label for="user">User Name</label>
                                    <input type="username"  id="username" required="required" name="username" class="form-control" placeholder="Username">
                               
                              
                            </div>
   
   <div class="form-group">
                                
                                  <label for="user">Employee Id</label>
                                    <input type="empid"  id="empid" required="required" name="empid" class="form-control" placeholder="Employee Id">
                               
                              
                            </div>
  <!--                           <div class="button">
   <button type="bu1tton" ng-click="profile()" class="btn btn-primary m-t-15 waves-effect">Submit</button>
  
    <button type="reset"  class="btn btn-default m-t-15 waves-effect">Reset</button>
     </div> -->
  <div class="resp-msg"></div>
    <div class="row button-demo">
                                
                
                                <button type="button" ng-if="showBtns" ng-hide="showSave " ng-click="profile()"  class="btn btn-primary waves-effect">Submit</button>
                               
                                <button type="button" ng-if="showBtns" ng-show="showSave"   ng-click="update($event)" id=""   name="update" class="btn btn-primary updatewarden waves-effect">Update</button>

                                <input   type="reset"   class="btn btn-default waves-effect" value="Reset">

                               

                                </div>
</form>

                     </div>
                     </div>
                     </div>    
                           


                          
                <div class="col-lg-7 col-md-6 col-sm-6   ">
                    <div class="card ">
                           
<div class="body">
      <h2>
                    Wardens Details       
                            
                            </h2>
                          
                       
                        <div class="body table-responsive" style="    padding-left: 1px;
">
                            <table class="table table-striped">
                                <thead align="center">
                                    <tr >
                                        <th>Sl_no</th>
                                        <th>Email_ID</th>
                                        <th>Sex</th>
                                        <th>User_Name</th>
                                        <th>Emp_Id</th>

                                        
                                        <th>Create_Date</th>
                                    </tr>
                                </thead>
                                <tbody >

                                  <tr ng-repeat="user in users" id="<%user._id%>" ng-if="user.delete=='1'">
                                  <th scope="row"><%(usersPerPage * (currentPage-1)) + $index+1  %></th>
                                    <td><% user.email %></td>
                                    <td ng-if="user.sex=='1'">M</td>
                                    <td ng-if="user.sex=='0'">F</td>
                                     <td><% user.username %> </td>
                                    <td><% user.empid %> </td>
                                    <td><% user.registered_on %> </td>
                                    <td>

            <button id="<%user._id%>" class="btn btn-info  btn-xs" ng-click="edit($event)">
                           Edit
                        </button>
                                    </td>
                                    <td>
                            <button id="<%user._id%>" class="btn btn-warning btn-xs" data-ng-click="delete($event)">
                            Delete
                        </button>
                                            


                                        </td>
                                </tr>
                                   
                                </tbody>
                            </table>
                            <div class="col-md-6 text-right">
                            <ul uib-pagination total-items="totalUsers" ng-model="currentPage" class="pagination-sm" boundary-links="true" rotate="false" max-size="maxSize" items-per-page="usersPerPage"></ul>
                          </div>
                        
                    </div>
                </div>
            </div>
            <!-- #END# Striped Rows -->


                             
                            </div>
                                    

            <!-- #END# Basic Examples -->

</div>
</div>
</div>
           </section>
          



<script src="{{asset('Superadmin/ng-controls/users_controls.js')}}"></script>
<script src="{{asset('Assets/parsley/parsley.js')}}"></script>
<script src="{{asset('Assets/jquery-confirm/jquery-confirm.js')}}"></script>
@endsection





<!--Button to Trigger Modal-->
<!-- div style="text-align:center; margin-top:10%">
<span   id="btnShow" class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
<button  id="btnShow" class="btn btn-primary btn-lg"> Show Modal Popup</button>
</div> -->
<!-- Modal -->






    