var myApp = angular.module('nightout', ['ngRoute', 'ui.bootstrap'], function($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>')
});

myApp.controller("UsersController", ['$scope', '$http', '$compile', function($scope, $http, $compile) {
    $(document).on('keyup', '[name=smart_query]', function(e) {
        // var code = (e.keyCode ? e.keyCode : e.which);
        // if (code == 13 || $(this).val()) {
            var myObject = {
              smart_query: $('[name=smart_query]').val(),
            };
            
            var cond = $.param( myObject );

            $scope.getUsers(1, cond);
        // }
    });
    
    // $(document).on('change', '[name=date_of_visit]', function(e) {
    //     var code = (e.keyCode ? e.keyCode : e.which);
    //     // if (code == 13 || $(this).val()) {
    //         var myObject = {
    //           status: $('[name=status]').val(),
    //           date_of_visit: $('[name=date_of_visit]').val(),
    //           smart_query: $('[name=search-visitor]').val()
    //         };
            
    //         var cond = $.param( myObject );

    //         $scope.getVisitors(1, cond);
    //     // }
    // });

    // $(document).on('change', '[name=status]', function(e) {
    //     var myObject = {
    //       status: $('[name=status]').val(),
    //       date_of_visit: $('[name=date_of_visit]').val(),
    //       smart_query: $('[name=search-visitor]').val()
    //     };
        
    //     var cond = $.param( myObject );

    //     $scope.getVisitors(1, cond);
    // });

    // Pagination of Users
    $scope.maxSize = 3;
    $scope.$watch("currentPage", function() {
        var myObject = {
          smart_query: $('[name=smart_query]').val(),
        };
                    
        var cond = $.param( myObject );

        $scope.getUsers($scope.currentPage, cond);
    });

    //load Users     
    $scope.getUsers = function(page=1, $cond='') {
        $http.get('/vms/users/data?page='+page+(($cond) ? ('&'+$cond) : ''))
            .then(function(res) {
                $scope.users = res.data.data;

                $scope.totalUsers = res.data.total;                
                $scope.currentPage = res.data.current_page;
                $scope.usersPerPage = res.data.per_page;
                $scope.usersFrom = res.data.from ? res.data.from : 0;
                $scope.usersTo = res.data.to ? res.data.to : 0;
        });
    }
}]);
