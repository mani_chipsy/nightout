$(function() {
    $.login = {
        init: function(callback) {
            $("[name=log_creditials] [name=btnsubmit]").click(function() {
                $.login.login($(this), callback);
            });
            $("[name=log_creditials] input").keyup(function(e) {
                var code = (e.keyCode ? e.keyCode : e.which);
                if (code == 13) {
                    $.login.login();
                }
                $.login.validate($(this), callback);
            });
        },
        validate: function(ths, callback) {
            if (ths.val() != "") {
                ths.css('border-color', '');
            }
            $('.message_area').html('');
        },
        login: function(ths, callback) {
            var proceed = true;
            $('[name=log_creditials] [required=true]').filter(function() {
                if ($.trim(this.value) == "") {
                    $(this).css('border-color', 'red');
                    proceed = false;
                }
            });
            if (proceed) {
                var postdata = {}
                var input = $('[name=log_creditials]').serializeArray();
                $.each(input, function() {
                    if (postdata[this.name] !== undefined) {
                        if (!postdata[this.name].push) {
                            postdata[this.name] = [o[this.name]];
                        }
                        postdata[this.name].push(this.value || '');
                    } else {
                        postdata[this.name] = this.value || '';
                    }

                });
                
                $.post('myfiles/controller/leads/user-login', postdata, function(response) { 
                        if (response.status.code==0) {
                        window.location = "/admin/dashboard.php";
                        } else {
                        $('.message_area').html(response.status.message).css('color', 'red');
                        $('[name=log_creditials] [name=password]').val('');

                    }
                }, 'json');
            }
        },

    }
    $.login.init();
})(jQuery);