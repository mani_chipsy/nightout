<?php
// checks required params
function required_filds($required_fields) {
    $error = false;
    $error_fields = "";
    $request_params = array();
	$request_params = $_REQUEST;
 
	foreach ($required_fields as $field) { 	
        if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
            $error = true;
            $error_fields .= $field . ', ';
        }
    }

    if ($error) {
        $response = array();
        $response['status']["code"] = 4;
        $response['status']["message"] = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
        die(json_encode($response));
    }
 }
 
 //check headers (user suthethication)
function auth() {
	  $headers = apache_request_headers();
	  if (isset($headers['Authorizations'])) {
	    require '../include/authethicateuser.php';
	    $res =$db->auth_user($headers['Authorizations']);
	  if($res==1){
	    $response['status']["code"] = 2024;
        $response['status']["message"] = 'Session expired.';
		 die(json_encode($response));
	   }
	  }else{
	    $response['status']["code"] = 1024;
        $response['status']["message"] = 'Authorizations not found.';
		 die(json_encode($response));
	  }
	 
}

 //check mails fromate
function validate_email($email) {
  if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
	     $response['status']["code"] = 5;
        $response['status']["message"] = 'Invalid email format';
        die(json_encode($response));
  }
}
//set expiry time
 function expiry_time(){
	 return 1800000;
 }
//checks mobile fromate 
function validate_mobile($mobile_number) {
	    	$mob="/^[6789][0-9]{9}$/";
			if(!preg_match($mob, $mobile_number))
			{	  $response['status']["code"] = 6;
				 $response['status']["message"] = 'Invalid mobile number!';
				 die(json_encode($response));
			}
 }
 

?>