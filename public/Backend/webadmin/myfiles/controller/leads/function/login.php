<?php
//user login base on table user
class login{
	private $db;
	private $conn;
	function __construct(){
	     $this->conn =  new ConnectDB();
	     $this->db= $this->conn->connectdb();

	}
	public function login_func(){
                        $response['status']['code']=1;
                        $response['status']['message']='Inavalid Username/Password ';
		        $user_name = filter_var($_POST["user_name"], FILTER_SANITIZE_STRING);
	                $password  = filter_var($_POST["password"], FILTER_SANITIZE_STRING);
                      
			$ency=new hashing_security(); //hash password
                       
			if($select_stm=$this->db->prepare('SELECT userId,user_name,password FROM my_admin WHERE user_name=?')){  
				$select_stm->bind_param('s',$user_name);
				$select_stm->execute();
				$select_stm->store_result();
				if($select_stm->num_rows>0){ 
					$select_stm->bind_result($user_id,$user_name,$hash_password);
					$select_stm->fetch();  
                                        $_SESSION['AUTH']['USER']  = $user_id;  
					if($ency->validate_password($password,$hash_password)){  
                                           $response['status']['code']=0;
                                           $response['status']['message']='Login sucessfully ';
					} 
                                  
				 }
			 }
		die(json_encode($response));
	}
}
?>