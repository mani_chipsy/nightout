
var myApp = angular.module('nightout', ['ngRoute', 'ui.bootstrap'], function($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>')
});


myApp.controller("VisitorsController", ['$scope', '$http', '$compile', function($scope, $http, $compile) {
    $(document).on('keyup', '[name=search-visitor]', function(e) {
        // var code = (e.keyCode ? e.keyCode : e.which);
        // if (code == 13 || $(this).val()) {

            var myObject = {
              status: $('[name=status]').val(),
              date_of_visit: $('[name=date_of_visit]').val(),
              smart_query: $('[name=search-visitor]').val()
            };
            
            var cond = $.param( myObject );

            $scope.getVisitors(1, cond);
        // }
    });
    
    $(document).on('change', '[name=date_of_visit]', function(e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        // if (code == 13 || $(this).val()) {
            var myObject = {
              status: $('[name=status]').val(),
              date_of_visit: $('[name=date_of_visit]').val(),
              smart_query: $('[name=search-visitor]').val()
            };
            
            var cond = $.param( myObject );

            $scope.getVisitors(1, cond);
        // }
    });

    $(document).on('change', '[name=status]', function(e) {
        var myObject = {
          status: $('[name=status]').val(),
          date_of_visit: $('[name=date_of_visit]').val(),
          smart_query: $('[name=search-visitor]').val()
        };
        
        var cond = $.param( myObject );

        $scope.getVisitors(1, cond);
    });

    // Pagination of Visitors

    $scope.maxSize = 3;
    $scope.$watch("currentPage", function() {
        var myObject = {
          status: $('[name=status]').val(),
          date_of_visit: $('[name=date_of_visit]').val(),
          smart_query: $('[name=search-visitor]').val()
        };

        var cond = $.param( myObject );

        $scope.getVisitors($scope.currentPage, cond);
    });

    //load Visitors     
    $scope.getVisitors = function(page=1, $cond='') {
        $http.get('/vms/visitors/data?page='+page+(($cond) ? ('&'+$cond) : ''))
            .then(function(res) {
                $scope.visitors = res.data.data;
               
                $scope.totalVisitors = res.data.total;                
                $scope.currentPage = res.data.current_page;
                $scope.visitorsPerPage = res.data.per_page;
                $scope.visitorsFrom = res.data.from ? res.data.from : 0;
                $scope.visitorsTo = res.data.to ? res.data.to : 0;
        });
    }

    $scope.previewCard = function($index) {
        var visitor = $scope.visitors[$index];

        var modal_body = $('#product_view').find('.modal-body');
        var body_content = '';

        body_content = '<div class="wrapper pass-preview pass">'
            +'<header>'
               +'<table align="center">'
                    +'<tr>'
                        +'<td class="brand-left"><img border="0" src="/Backend/webadmin/images/tapmi-logo.png"/></td>'
                        +'<td><h2>T A PAI MANAGEMENT INSTITUTE - MANIPAL</h2></td>'
                        +'<td class="brand-right"><img border="0" src="/Backend/webadmin/images/tapmi-AACSB.png"  /></td>'
                    +'</tr>'
                +'</table>'
            +'</header>' 
            +'<div class="maincontent">'
                +'<h3 style="text-align: center; text-decoration: underline;">Visitor\'s Pass</h3>'
                +'<table align="center" width="100%">'
                    +'<tr>'
                        // +'<td width="15%" >Name</td>'
                        // +'<td width="20%" ><div style="padding:0.5em;border-bottom: 1px solid black">'+visitor.name+'</div></td>'
                        // +'<td width="10%" style="border: none">&nbsp;</td>'
                        // +'<td width="10%" align="right" >Mobile Number</td>'
                        // +'<td width="30%" colspan=2 ><div style="padding:0.5em;border-bottom: 1px solid black">'+visitor.mobile_num+'</div></td>'
                        +'<td width="15%" >Name</td>'
                        +'<td width="20%" ><div style="padding:0.5em;border-bottom: 1px solid black">'+visitor.name+'</div></td>'
                        +'<td width="20%" align="right" >Mobile Number</td>'
                        +'<td width="30%" colspan=2 ><div style="padding:0.5em;border-bottom: 1px solid black">'+visitor.mobile_num+'</div></td>'
                    +'</tr>'
                    +'<tr>'
                        +'<td width="25%" >Address</td>'
                        +'<td width="75%" colspan=5  style="border-bottom: 1px solid black">'+visitor.address+'</td>'
                    +'</tr>'
                    +'<tr>'
                        +'<td width="25%" >Requested by</td>'
                        +'<td width="75%" colspan="5"  style="border-bottom: 1px solid black">'+((visitor.requester) ? visitor.requester.username : 'Security')+'</td>'
                    +'</tr>'
                    +'<tr>'
                        +'<td width="25%"  >Whom to meet in TAPMI</td>'
                        +'<td width="75%" colspan=5  style="border-bottom: 1px solid black">'+visitor.whom_to_meet+'</td>'
                    +'</tr>'
                    +'<tr>'
                        +'<td width="25%" >Purpose of visit</td>'
                        +'<td width="75%" colspan=5  style="border-bottom: 1px solid black">'+visitor.purpose+'</td>'
                    +'</tr>'
                    +'<tr>'
                        +'<td width="15%" >Date of visit</td>'
                        +'<td width="75%" colspan=5  style="border-bottom: 1px solid black">'+visitor.date_of_visit+'</td>'
                        // +'<td width="10%" align="right">Time in</td>'
                        // +'<td width="15%" style="border-bottom: 1px solid black">&nbsp;</td>'
                        // +'<td width="10%"  align="right">Time out</td>'
                        // +'<td width="15%" style="border-bottom: 1px solid black">&nbsp;</td>'
                    +'</tr>'
                +'</table>'
                +'<div class="row-padding" style="margin-top:60px" ><div class="row">'
                    +'<div class="col-md-6 row-padding">Photo 1.</div>'
                    +'<div class="col-md-6 row-padding">Photo 2.</div>'
                +'</div>'
                +'<div class="row" >'
                    +'<div class="col-md-6"><img class="img-responsive" style="height:500px;border:1px solid #021a40" src="/'+((visitor.visitor_img) ? visitor.visitor_img : ('Backend/webadmin/images/vistor-photo.png'))+'"  /></div>'
                    +'<div class="col-md-6"><img class="img-responsive" style="height:500px;border:1px solid #021a40" src="/'+((visitor.visitor_id_img) ? visitor.visitor_id_img : ('Backend/webadmin/images/vistor-photo.png'))+'"  /></div>'
                +'</div>'
            +'</div>'
            +'</div>'
        +'</div>';
        
        modal_body.html(body_content);

        $('#product_view').modal('show');
    }
}]);
