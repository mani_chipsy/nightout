<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




// Route::get('/hash', 'UserController@run');
// die(json_encode("value"));
Route::group(['middleware' => ['guest:nightout']], function () {
    Route::get('/login','Auth\AuthController@index');
    Route::get('/', function () {
        return redirect('/nightout/pgp-office/login');
    });
     Route::post('auth/postlogin', 'Auth\AuthController@postLogin');
});
// Route::get('/', 'Auth\AuthController@index');


	 


Route::get('/signout', 'Auth\AuthController@signout');

Route::group(['middleware' => ['nightout']], function () {
	 Route::get('/settings', 'Auth\AuthController@settings_view');
	  Route::post('/sub-settings', 'Auth\AuthController@change_settings');
	 Route::get('display', 'Admin_Pannel@nightout_func');
	Route::get('display/{render}', 'Admin_Pannel@nightout_func');
	 Route::post('/status', 'Admin_Pannel@change_status');

 // Route::get('request', 'Admin_Pannel@nightout_welcome');
Route::get('request', 'Admin_Pannel@Nightout_welcome');
 Route::get('/request/{render}/{pgp}', 'Admin_Pannel@Nightout_welcome');
Route::post('/action', 'Admin_Pannel@change_action');
  Route::get('/dashbord', 'Admin_Pannel@dashboard_func');




});




