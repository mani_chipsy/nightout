<?php

Route::get('/signout', 'AuthController@signout');

Route::group(['middleware' => ['guest']], function () {
    Route::get('/login', [ 'as' => 'login', 'uses' => 'AuthController@index']);
    Route::get('/', function () {
        return redirect('/login');
    });
    Route::post('/auth-login', 'AuthController@auth_login');
});
    

Route::group(['middleware' => ['auth']], function () {
    Route::get('/settings', 'AuthController@settings_view');
    Route::post('/sub-settings', 'AuthController@change_settings');
    
    Route::group(['prefix' => 'vms'], function () {
        Route::get('/home', 'VMSController@dashboard_func');
        Route::get('/visitors', 'VMSController@visitors_func');
        Route::get('/visitors/{data}', 'VMSController@visitors_func');
        Route::get('/users', 'VMSController@users_func');
        Route::get('/users/{data}', 'VMSController@users_func');

        Route::get('/print-pass/{visitor_id}', 'VMSController@print_pass');
    });
});

