<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace' => 'Web\Backend'], function () {
    require(__DIR__ . '/Routes/Web/vms_controls.php');
});

Route::group(['namespace' => 'Web\Nightout','prefix' => 'nightout/pgp-office'], function () {

    require(__DIR__ . '/Routes/Web/nightout_controls.php');
});

Route::group(['namespace' => 'Web\Superadmin','prefix' => 'superadmin'], function () {

    require(__DIR__ . '/Routes/Web/superadmin_controls.php');
});

Route::group(['namespace' => 'Web\Warden','prefix' => 'warden'], function () {

    require(__DIR__ . '/Routes/Web/warden_controls.php');
});

Route::group(['namespace' => 'Web\Office','prefix' => 'office'], function () {

    require(__DIR__ . '/Routes/Web/office_controls.php');
});