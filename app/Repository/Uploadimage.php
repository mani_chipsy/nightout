<?php
namespace App\Repository;

use App\Http\Controllers\Controller;
use Image;

class Uploadimage extends Controller
{
    public static function upload_func($userId, $img_cat, $img)
    {
        $i_year     =date('Y');
        $i_month    =date('m');
        $image_name =$userId.uniqid();
      
        $imgtime=time();
       
        if (!is_dir("Backend/". $img_cat)) {
            mkdir("Backend/". $img_cat);
        }

        if (!is_dir("Backend/". $img_cat .  '/' . $i_year)) {
            mkdir("Backend/". $img_cat . '/'. $i_year);
        }

        if (!is_dir("Backend/". $img_cat . '/'.$i_year . "/" . $i_month)) {
            mkdir("Backend/". $img_cat  . '/'. $i_year. "/" . $i_month);
        }
        
        list($width, $height) = getimagesize($img);

        $ratio = $width/$height; // width/height

        if ($ratio > 1) {
            $width = 150;
            $height = 150/$ratio;
        } else {
            $width = 150*$ratio;
            $height = 150;
        }

        $ext = $img->getClientOriginalExtension();
        $path['path'] ='Backend/' . $img_cat . '/' . $i_year . '/' . $i_month . '/' .$imgtime . "_" . $image_name;

        Image::make($img)->save($path['path'].'_s.'.$ext);

        if ($ratio > 1) {
            $width = 300;
            $height = 300/$ratio;
        } else {
            $width = 300*$ratio;
            $height = 300;
        }
        Image::make($img)->save($path['path'].'_l.'.$ext);

        $visitors_img=$path['path'].'_l.'.$ext;
        return $visitors_img;
    }
}
