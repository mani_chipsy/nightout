<?php
namespace App\Repository\Frontend\Auth;

use DB;
use App\Http\Controllers\Controller;
use Auth;
use Hash;

class Forgot extends Controller
{
    public static function forgot_func($data)
    {
        $getuser = DB::collection('users')->where('email', $data['forgot_email'])->Where('roll_id', '!=', 100)
            ->select('_id', 'profile_img')
            ->first();

        $response['status']['code'] = 1;
        $response['status']['message'] = "We couldn't find an account associated with " . $data['forgot_email'];

        if (count($getuser) > 0) {
            $forgot_code = rand(100000, 999999);
            $forgot_code = 111111;
            $redirect_url = uniqid();
            $expirytime = strtotime("+30 minutes");
            $forgot_code_hash = base64_encode($forgot_code);
            $userId = $getuser["_id"];
            $validtime = time() - (5 * 60); // not equal to les than 5 minitus .
            $count = DB::table('forgot_password')->where('userId', $userId)->where('expirytime', '>', $validtime)->get(array(
                'forgotAuthKey',
                'redirect_url'
            ));

            if (count($count) == 0) {
                DB::collection('forgot_password')->where('userId', $userId)->delete();
                DB::collection('forgot_password')
                    ->insertGetId(['userId' => $userId, 'forgotAuthKey' => $forgot_code_hash, 'redirect_url' => $redirect_url, 'verifiedCode' => '', 'expirytime' => $expirytime, ]);
            } else {
                $redirect_url = $count[0]['redirect_url'];
                $forgot_code = base64_decode($count[0]['forgotAuthKey']);
            }

            $response['status']['code'] = 0;
            $response['status']['message'] = 'OTP send to your to your email. ';
            $response['data']['redirect_url'] = $redirect_url;
            return $response;
        }

        return $response;
    }

    public static function forgotVerify_func($redirect_url)
    {
        $res = DB::collection('forgot_password')->where('redirect_url', $redirect_url)->get(array(
            'userId'
        ));

        if (count($res) > 0) {
            $user_res = DB::collection('users')->where('_id', $res[0]['userId'])
                ->get(array(
                'email',
                'profile_img'
            ));
            return $user_res;
        }
    }

    public static function forgototpVerify_func($data)
    {
        $opt = base64_encode($data['forgot_otp']);
        $count = DB::collection('forgot_password')->where('redirect_url', $data['verify_code'])->where('forgotAuthKey', $opt)->count();

        $response['status']['code'] = 1;
        $response['status']['message'] = "The number that you've entered doesn't match your code. Please try again ";
        if ($count == 1) {
            $verifiedCode = uniqid();
            DB::collection('forgot_password')->where('redirect_url', $data['verify_code'])->update(['verifiedCode' => $verifiedCode]);

            $response['status']['code'] = 0;
            $response['status']['message'] = 'OTP is verified. ';
            $response['data']['verifiedCode'] = $verifiedCode;
        }
        return $response;
    }

    public static function forgotUpdate_func($data)
    {
        $res = DB::collection('forgot_password')->where('verifiedCode', $data['verifiedCode'])->get();
        DB::collection('forgot_password')
            ->where('verifiedCode', $data['verifiedCode'])->delete();
        $password = Hash::make($data['password']);

        DB::collection('users')->where('_id', $res[0]['userId'])->update(['password' => $password]);
        Auth::loginUsingId($res[0]['userId']);

        $response['status']['code'] = 0;
        $response['status']['message'] = 'You have successfully updated the password.';

        return $response;
    }
}
