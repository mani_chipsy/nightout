<?php
namespace App\Repository\Frontend\Auth;

use DB;
use App\Http\Controllers\Controller;
use Auth;
use Hash;

class Register extends Controller
{
    public static function register_func($data)
    {
        $id=DB::collection('users')->insertGetId([ 'roll_id'      => 2,
            'user_type_id'  => $data['user_type'],
            'email'       => $data['email'],
            'password'    =>  Hash::make($data['password']),
            'mobile_number'=>  null,
            'first_name'   =>  $data['first_name'],
            'last_name'    =>  $data['last_name'],
            'remember_token' => null,
            'profile_img'=>'default_user.png',
            'cover_image'=>'def_cover.png',
            'cover_image_pos'=>'0',
            'expires_in'   =>  null,
            'linkedin_id'    => null,
            'active_state'=> 0,
            'sink_state'  => 0,
            'name'        => null,
            'avatar'      => null,
            'headline'    =>  null,
            'logby'       => 'web',
            'industry'    => null,
            'public_profile_url' => null,
            'avatar_original'  => null,
            'address'=> null,
            'created'   => time(),
            ]);

        $p_settings = DB::collection('privacy_settings')->where('status', 1)->get();

        foreach ($p_settings as $value) {
            DB::collection('user_privacies')->insert([
                'user_id'      => (string)$id,
                'privacy_setting_id'  => (string)$value['_id'],
                'checked'       => 1
            ]);
        }

        $n_settings = DB::collection('noty_settings')->where('status', 1)->get();

        foreach ($n_settings as $value) {
            DB::collection('user_notifications')->insert([
                'user_id'      => (string)$id,
                'noty_setting_id'  => (string)$value['_id'],
                'checked'       => 1
            ]);
        }

        Auth::loginUsingId($id);
         
        return response()->json(array(
            'success' => true,
            'message' => "Registertion sucessfull.",
            'redirect' => '/add-profile'
        ));
    }
}
