<?php
namespace App\Repository\Frontend\Profile;

use DB;
use App\Http\Controllers\Controller;
use Auth;
use Carbon\Carbon;

class GetProfile extends Controller
{
    public static function get_profile_func()
    {
        $userid=Auth::user()->_id;
        $user = Auth::user();

        $user['dob'] = ($user['dob']) ? date('d-m-Y', $user['dob']) : '';
        $user['want_to_be'] = explode(',', $user['want_to_be']);
        $response['data']['Personal']   = $user;

        $response['data']['work']       =DB::collection('work_details')->where('userId', $userid)->get();
        $response['data']['education']  =DB::collection('education_details')->where('userId', $userid)->get();
        $response['data']['skills']     =DB::collection('skill_details')->where('userId', $userid)->get();
        $response['status']['code']     =0;
        $response['status']['message']  ='Profile details.';
        return $response;
    }

    public static function elapsed_time($time)
    {
        $time = time() - $time; // to get the time since that moment
        $time = ($time<1)? 1 : $time;
        $tokens = array(
            31536000 => 'year',
            2592000 => 'month',
            604800 => 'week',
            86400 => 'day',
            3600 => 'hour',
            60 => 'minute',
            1 => 'second'
        );

        foreach ($tokens as $unit => $text) {
            if ($time < $unit) {
                continue;
            }
            $numberOfUnits = floor($time / $unit);
            return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'s':'').' ago';
        }
    }
}
