<?php 
namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Auth;

class warden
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
  
  public function handle($request, Closure $next)
    {   
       //   $auth=Auth::guard('superadmin');
       // if(Auth::check()){
 
      if (Auth::guard("warden")->check()) {
       
          if (Auth::guard("warden")->user()->roleId==1001) {

            return $next($request);

               
            } }
            else{

                 return redirect('/warden/login');

                  }
        
        
    }
}
