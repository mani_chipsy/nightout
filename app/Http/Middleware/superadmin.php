<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Auth;

class superadmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
   

    public function handle($request, Closure $next)
    {   
       //   $auth=Auth::guard('superadmin');
       // if(Auth::check()){
  // die(json_encode(Auth::user()));
      if (Auth::guard("superadmin")->check()) {

                if (Auth::guard("superadmin")->user()->roleId==1003) {

            return $next($request);

               
            } }
            else{die(json_encode("value"));

                 return redirect('/superadmin/login');

                  }
        
        
    }
}
