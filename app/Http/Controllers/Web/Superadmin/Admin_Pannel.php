<?php 
namespace App\Http\Controllers\Web\Superadmin;
use App\Http\Controllers\Controller;
use App\User;
use App\PgStudents;
use App\RequestNightOut;
use Illuminate\Http\Request;
use Validator;
use DB;
use Hash;
use Auth;
use Input;
class Admin_Pannel extends Controller {

   

    public function dashboard_func(Request $request)
    {
        $return = [];
        $return['total_users'] = PgStudents::count();
        $return['pending'] = RequestNightOut::whereNotIn('status',[-1,2])->count();
        $return['aproved'] = RequestNightOut::where('status', 2)->count();
        $return['rejected'] = RequestNightOut::where('status', -1)->count();

        return view('Nightout.superadmin.page.dashboard', $return);
    }
	 	
    //          public function dashboard_func(Request $request)
    // {
       


    //     return view('Nightout.superadmin.page.dashboard');
    // }
          	
          	   public function create_func()
            {
              
          return view('Nightout.superadmin.page.user' );
            }
          	public function Nightout_display()
          	{

          		
           $data = DB::collection('request_nightout')->get();
                  return $data;
              }
	

  public function user_func(Request $request)
  {
    $rules     = array(
            'username' => 'required',
            'email' => 'required',
            'empid' => 'required',
            'group1'=>'required'
            
        );
        
        $change_pass = @$request->all()['change_pass'];

        if (@$request->all()['change_pass']) {
            $rules['new_password'] = 'required';
            $rules['confirm_password'] = 'required|same:new_password';
        }

        $messages = [
            'confirm_password.same' => 'Confirm Password should match the New Password',
            'confirm_password.required' => 'Confirm Password is required',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        $data      = $request->all();

        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'message' => $validator->getMessageBag()->toArray()
            ));
        }

        $roleId= 1001;
        $delete=1;

         
           $user = User::where('email', '=', $request->email)->first();
if ($user === null) {
   // user doesn't exist

  $user = new User;
             $n_password = Hash::make($request->new_password);
              $user->username=$request->username;
                $user->password = $n_password;
                $user->sex=$request->group1;
                $user->email=$request->email;
                $user->empid=$request->empid;
                 $user->roleId=$roleId;
                $user->delete=$delete;
                 $user->save();
                  return response()->json(array(
                'success' => true,
                'message' => 'Success',
            ));
                }

            else
                {
                return response()->json(array(
                'success' => false,
                'message' => 'Email Id already exitsted', ));
              }


  }


  public function update_func(Request $request,$id)
  {
    $rules     = array(
            'username' => 'required',
            'email' => 'required',
            'empid' => 'required',
            'group1'=>'required'
            
        );
        
        $change_pass = @$request->all()['change_pass'];

        if (@$request->all()['change_pass']) {
            $rules['new_password'] = 'required';
            $rules['confirm_password'] = 'required|same:new_password';
        }

        $messages = [
            'confirm_password.same' => 'Confirm Password should match the New Password',
            'confirm_password.required' => 'Confirm Password is required',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        $data      = $request->all();

        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'message' => $validator->getMessageBag()->toArray()
            ));
        }

        // $roleId= 1001;
        // $delete=1;
        // echo $id;

            $user =User::where('_id', $id)->first();
           
              $user->username=$request->username;
              if($request->new_password!="********")
              {
                 $n_password = Hash::make($request->new_password);
                 $user->password = $n_password;
              }
                
                $user->sex=$request->group1;
                $user->email=$request->email;
                $user->empid=$request->empid;
                 // $user->roleId=$roleId;
                // $user->delete=$delete;
                 $user->save();

                  return response()->json(array(
                'success' => true,
                'message' => 'successfully updated',
            ));
   
         
           

  }

  public function edituser_details(Request $request, $id)
            {
              $user = User::where('_id', '=', $id)->first();
              
             
               $user->password="********";

               $user->password_str="********";
           
             
              return $user;
            }
        

   public function display_func(Request $request, $render='view')
    {
        if ($render == 'view') {
 

            return view('Nightout.superadmin.page.nightout', ['current_day' => date('d-m-Y')]);
        } elseif ($render=="pending") {

         $visitors = RequestNightOut::whereNotIn('status',[-1,2])->with('requester','user')->orderBy('created_at', 'DESC')->paginate(10);

                foreach ( $visitors as  $nightout) {
                        $nightout->requested_on=date('d-m-Y',$nightout->toDate);
                         
                        $nightout->requested_on1= date('d-m-Y',$nightout->fromDate);

                         
                    } 
                    $visitors->st=0;

            return $visitors;
        }
        else
        {
            $query = $request->query();

            $visitors = RequestNightOut::query()->with('requester','user');
            // die(json_encode($query['smart_query']));
if ($cond = @$query['smart_query']) {
                $visitors->whereHas('requester', function($q) use ($cond){
                    $q->orWhere('name', 'like', '%'.$cond.'%');
                    $q->orWhere('rollNo', 'like', '%'.$cond.'%');
                    // $q->orWhere('address', 'like', '%'.$cond.'%');
                });
}
            if (isset($query['status']) && $query['status'] != '') {
                $visitors->where('status', (int)$query['status']);

            }

                
              // $visitors->where(function ($q) use ($cond) {
                    
              //   });
            


            if (@$query['date_of_visit']) {
              
                  $date_of_visit= strtotime($query['date_of_visit']);

            
                $visitors->where('fromDate', $date_of_visit);
            }
  
            $visitors = $visitors->whereNotIn('status',['-1','2'])->orderBy('created_at', 'DESC')->paginate(10);
           
          
                 foreach ( $visitors as  $nightout) {
                        $nightout->requested_on=date('d-m-Y',$nightout->toDate);
                         
                        $nightout->requested_on1= date('d-m-Y',$nightout->fromDate);

                       // echo $nightout;
                    }


            return $visitors;
        }
    }

 public function nightout_func(Request $request, $render='view')
            {
            
                if ($render == 'view') {
 

            return view('Nightout.superadmin.page.students' );
        }  elseif ($render!="data") {


               $nightouts =PgStudents::where('pgp',$render)->paginate(10);  
                  }
                  else
                  {
              $nightouts =PgStudents::paginate(10);  
                              
               
                  }


                 
             return $nightouts;
           
          }


    public function user_details(Request $request)
    {
        $roleId=1001;
            

            $users = User::where('roleId' ,$roleId  )->where('delete' ,1)->paginate(10);

           
           

            foreach ($users as $user) {
                $user->registered_on = date('d-m-Y', strtotime($user->created_at));
            }

            return $users;
        
    }

         public function delete_func(Request $request)
         {
          $id=$request->id;
          $delete=0;

         
          $user = User::where('_id',$id)->first();
              
             $user->delete =$delete ;
             $user->save();
             
             return response()->json(array(
                              'success'=>"true",
                              
                              'message' => " successfully delete"
                              ));
            
          }
   
	




	

}
