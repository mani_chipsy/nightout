<?php
namespace App\Http\Controllers\Web\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use DB;
use Auth;
use Session;
use Hash;
use App\User;

class AuthController extends Controller
{
    public function index(Request $request)
    {
        return view('Backend.login');
    }

    public function auth_login(Request $request)
    {
        $inputs    = $request->all();

        $rules     = array(
            'username' => 'required|max:255',
            'password' => 'required'
        );

        $validator = Validator::make($request->all(), $rules);

        $data      = $request->all();

        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'message' => $validator->getMessageBag()->toArray()
            ));
        }
        
        $creds = [
            'username'          => $data['username'],
            'password'       => $data['password'],
            "roleId"       => 1000,
            
        ];

        if (Auth::attempt($creds, true)) {
            return redirect('/vms/visitors');
        } else {
            
            Session::flash('login-error', 'Invalid username or password');
            Session::flash('loggd-username', $data['username']);

            return redirect('/login');
        }
    }

    public function settings_view()
    {
        return view('Backend.pages.settings');
    }

    public function change_settings(Request $request)
    {
        $rules     = array(
            'username' => 'required',
            'current_password' => 'required'
        );
        
        $change_pass = @$request->all()['change_pass'];

        if (@$request->all()['change_pass']) {
            $rules['new_password'] = 'required';
            $rules['confirm_password'] = 'required|same:new_password';
        }

        $messages = [
            'confirm_password.same' => 'Confirm Password should match the New Password',
            'confirm_password.required' => 'Confirm Password is required',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        $data      = $request->all();

        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'message' => $validator->getMessageBag()->toArray()
            ));
        }


        if (Hash::check($data['current_password'], Auth::user()->password)) {
            $user = User::find(Auth::id());

            if ($change_pass) {
                $n_password = Hash::make($data['new_password']);
                $user->password = $n_password;
            }

            $user->username = $data['username'];
            $user->save();
            // Auth::logout();
            Auth::loginUsingId(Auth::id());

            return response()->json(array(
                'success' => true,
                'message' => 'Success',
            ));
        } else {
            return response()->json(array(
                'success' => false,
                'message' => 'Current password entered is incorrect',
            ));
        }
    }

    public function signout()
    {
        Auth::logout();

        return redirect('/');
    }

}
