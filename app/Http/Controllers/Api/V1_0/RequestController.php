<?php
namespace App\Http\Controllers\Api\V1_0;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\RequestNightOut;
Use App\Pg_Students;
class RequestController extends Controller {

	public function request_nightout(Request $request){

        $rules = array('placeOfVisit'=>'required',
                       'purposeVisit'=>'required',
                       'fromDate'=>'required',
                       'toDate'=>'required',
                       'toDate'=>'required',
                       'hosteNumber'=>'required',
                       'roomNumber'=>'required',
                       'alternativeNumber'=>'required',
                       'numberOfPersons'=>'required',
                       'numberOfaffidavit'=>'required',
                    );
        $validator = Validator::make($request->all(), $rules);
        $data = $request->all();
      
        if ($validator->fails()) {
            $response['status']['code']=1;
            $res=$validator->getMessageBag()->toArray();
            foreach ($res as $key => $value) {
                $response['status']['message']=$value[0];
            }
            return response()->json($response);
        }

          $cur_time   = time();
          $current_time = date('h:i a', strtotime($cur_time));
          $sunrise = "9:30 am";
          $sunset = "5:00 pm";
          $date1 = DateTime::createFromFormat('H:i a', $current_time);
          $date2 = DateTime::createFromFormat('H:i a', $sunrise);
          $date3 = DateTime::createFromFormat('H:i a', $sunset);
          if ($date1 < $date2 && $date1 > $date3){
            $response['status']['code'] = 500;
            $response['status']['message']= "Please request Between 9:30 AM to 5:00 PM";
          return response()->json($response,200);
          }
                           

        $userId=$GLOBALS['userId'];
        $student=Pg_Students::where('_id',$userId)->get();
        $termCredits=$student->termCredits
        $placed=$student->placed
        if($termCredits<="0" && $placed=="0" ){        
          $response['status']['code'] = 501;
          $response['status']['message']= "You have reached max nightout";
          return response()->json($response,200);
        }
        $count=$student->count
        if($termCredits<="0"){        
          $response['status']['code'] = 502;
          $response['status']['message']= "You have reached max Request";
          return response()->json($response,200);
        }
        $student->count=$count-1;
        $student->save();

        $request_out = new RequestNightOut;
        $request_out->user_id =$userId;
        $request_out->status = 0;
        $request_out->placeOfVisit = $data['placeOfVisit'];
        $request_out->purposeVisit = $data['purposeVisit'];
        $request_out->fromDate = strtotime($data['fromDate']);
        $request_out->toDate   =  strtotime($data['toDate']);
        $request_out->hosteNumber = $data['hosteNumber'];
        $request_out->roomNumber = $data['roomNumber'];
        $request_out->alternativeNumber = $data['alternativeNumber'];
        $request_out->numberOfaffidavit = $data['numberOfaffidavit'];
        $request_out->numberOfPersons = $data['numberOfPersons'];
        $request_out->lateEntry = array();
        $request_out->wardenComment =  "";
        $res = $request_out->save();  
        $data['_id']=$request_out->_id;
        $data['lateEntry']=array();
        $data['wardenComment']=array();
        $response['status']['code']            = 0;
        $response['status']['message']         = "Request Booked successfully.";
        $response['data'] =$data;
	      return response()->json($response,200);



	}


    public function get_request_nightout(Request $request){
     
        $data=RequestNightOut::get();
        $response['status']['code']            = 0;
        $response['status']['message']         = "Requested  details.";
        
        foreach($data as &$list){
			$list['fromDate']=date('d-m-Y',$list['fromDate']);
			$list['toDate']=date('d-m-Y',$list['toDate']);
			
		}
        $response['data'] =$data;
        return response()->json($response,200);
    }

     public function get_request_id(Request $request,$id){
     
        $data=RequestNightOut::find($id);
         
			 $data->fromDate=date('d-m-Y',$data->fromDate);
			 $data->toDate=date('d-m-Y',$data->toDate);
			
			 
		
        $response['status']['code']            = 0;
        $response['status']['message']         = "Requested  details.";
        $response['data'] =$data;
        return response()->json($response,200);
    }

     public function post_laste_entry(Request $request){
        $rules = array('lateTime'=>'required',
                     'reason'=>'required',
                     '_id'=>'required',
                    );
        $validator = Validator::make($request->all(), $rules);
        $data = $request->all();
        if ($validator->fails()) {
            $response['status']['code']=1;
            $res=$validator->getMessageBag()->toArray();
            foreach ($res as $key => $value) {
                $response['status']['message']=$value[0];
            }
            return response()->json($response);
        }
         $cur_time   = time();
          $current_time = date('h:i A', strtotime($cur_time));
          $lateTime = "10:30 pm";
           $date1 = DateTime::createFromFormat('H:i a', $current_time);
          $date2 = DateTime::createFromFormat('H:i a', $lateTime);
          if($date1>$date2){
           $response['status']['code'] = 503;
            $response['status']['message']= "Please request 10:30 PM";
          return response()->json($response,200);

          }

         $info['lateTime']=$data['lateTime'];
         $info['reason']=$data['reason'];
     
         RequestNightOut::where('_id',$data['_id'])
              ->update(['lateEntry' => array($info)]);

        
        $response['status']['code']            = 0;
        $response['status']['message']         = "Requested  details.";
        $response['data'] =$data;
        return response()->json($response,200);
    }
   
	 

}
