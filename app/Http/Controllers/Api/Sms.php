<?php


define("GATEWAY_HOST", "sms6.routesms.com");
define("CURRENT_PORT", "");
define("SMS_USERNAME", "chipsyinfo");
define("SMS_PASSWORD", "chip2015");
define("SMS_SENDER", "TAPMIS");
define("GATEWAY_MSGTYPE", "0");
define("GATEWAY_DLR", "1");

class Sms
{
    public $host;
    public $port;
    public $strUserName;
    public $strPassword;
    public $strSender;
    public $strMessage;
    public $strMobile;
    public $strMessageType;
    public $strDlr;
    private function sms__unicode($message)
    {
        $hex1='';
        if (function_exists('iconv')) {
            $latin = @iconv('UTF-8', 'ISO-8859-1', $message);
            if (strcmp($latin, $message)) {
                $arr = unpack('H*hex', @iconv('UTF-8', 'UCS-2BE', $message));
                $hex1 = strtoupper($arr['hex']);
            }
            if ($hex1 =='') {
                $hex2='';
                $hex='';
                for ($i=0; $i < strlen($message); $i++) {
                    $hex = dechex(ord($message[$i]));
                    $len =strlen($hex);
                    $add = 4 - $len;
                    if ($len < 4) {
                        for ($j=0;$j<$add;$j++) {
                            $hex="0".$hex;
                        }
                    }
                    $hex2.=$hex;
                }
                return $hex2;
            } else {
                return $hex1;
            }
        }
    }
    public function send($sms_message, $mobile_no='')
    {
        $this->host=GATEWAY_HOST;
        $this->port=CURRENT_PORT;
        $this->strUserName = SMS_USERNAME;
        $this->strPassword = SMS_PASSWORD;
        $this->strSender= SMS_SENDER;
        $this->strMessage=$sms_message;
        $this->strMobile= $mobile_no;

        $this->strMessageType=GATEWAY_MSGTYPE;
        $this->strDlr=GATEWAY_DLR;
        $this->strMessage=urlencode($this->strMessage);
        try {
            $live_url="http://".$this->host.":".$this->port."/bulksms/bulksms?username=".$this->strUserName."&password=".$this->strPassword."&type=".$this->strMessageType."&dlr=".$this->strDlr."&destination=".$this->strMobile."&source=".$this->strSender."&message=".$this->strMessage."";
            $parse_url=file($live_url);
            // file_put_contents('sms_response.txt', print_r($parse_url, true));

            $result=$parse_url[0];
            $response_code=array();
            $response_code = explode("|", $result);
            return $response_code[0];
        } catch (Exception $e) {
        }
    }
}
