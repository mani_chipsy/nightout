<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Visitor;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // Delete visitor entries and photos past 30 days
        // $schedule->command('archive:visitor')->everyMinute();
        $schedule->call(function () {
            $visitors = Visitor::where('dov_date', '<', strtotime('-30 days 00:00:00'))->get();

            foreach ($visitors as $visitor) {
                $small_img = '';
                $lrg_img = '';

                if ($visitor->visitor_img) {
                    $name = explode('/', $visitor->visitor_img);
                    $name = array_pop($name);
                    $ext = explode('.', $name);
                    $ext = array_pop($ext);
                    $parts = explode('_', $name);
                    $small_img = public_path(strchr($visitor->visitor_img, $name, true).$parts[0].'_'.$parts[1].'_s.'.$ext);

                    if (file_exists($small_img)) {
                        unlink($small_img);
                    }

                    $lrg_img = public_path(strchr($visitor->visitor_img, $name, true).$parts[0].'_'.$parts[1].'_l.'.$ext);

                    if (file_exists($lrg_img)) {
                        unlink($lrg_img);
                    }
                }

                if ($visitor->visitor_id_img) {
                    $name = explode('/', $visitor->visitor_id_img);
                    $name = array_pop($name);
                    $ext = explode('.', $name);
                    $ext = array_pop($ext);
                    $parts = explode('_', $name);
                    $small_img = public_path(strchr($visitor->visitor_id_img, $name, true).$parts[0].'_'.$parts[1].'_s.'.$ext);

                    if (file_exists($small_img)) {
                        unlink($small_img);
                    }
                    $lrg_img = public_path(strchr($visitor->visitor_id_img, $name, true).$parts[0].'_'.$parts[1].'_l.'.$ext);

                    if (file_exists($lrg_img)) {
                        unlink($lrg_img);
                    }
                }
            }

            Visitor::where('dov_date', '<', strtotime('-30 days 00:00:00'))->delete();
        })->daily();

        // $schedule->command('inspire')
        //          ->hourly();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
