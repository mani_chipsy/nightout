<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Model;

class PgStudents extends Model
{
    protected $collection = 'pg_students';
    
    public function requester()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
